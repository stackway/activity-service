using System;
using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace ActivityService.Infrastructure.RabbitMQ {
    public interface IRabbitMqSender<TM> {
        void Send(TM message);
        void Send(string message);
    }

    public class RabbitMqSender<TM> : IRabbitMqSender<TM>, IDisposable {
        private readonly IRabbitMqSenderRoute<TM> _route;
        private readonly IModel _channel;

        private readonly ILogger _logger;

        public RabbitMqSender(
            IConnection connection,
            IRabbitMqSenderRoute<TM> route,
            ILogger<RabbitMqSender<TM>> logger
        ) {
            _route = route;
            _logger = logger;
            _channel = connection.CreateModel();

            SetupRabbitMqRoute();
        }

        private void SetupRabbitMqRoute() {
            _channel.ExchangeDeclare(
                _route.Exchange,
                RabbitMqExchangeTypeResolver.Resolve(
                    _route.ExchangeType
                )
            );
        }

        public void Send(TM message) {
            var serializedMessage = JsonConvert.SerializeObject(message);
            var body = Encoding.UTF8.GetBytes(serializedMessage);

            var props = _channel.CreateBasicProperties();
            props.ContentType = _route.ContentType;

            _channel.BasicPublish(
                _route.Exchange,
                _route.RoutingKey,
                props,
                body
            );

            _logger.LogInformation(
                "[-] message published({route} as {type}): {message}",
                _route.Exchange,
                _route.ExchangeType,
                serializedMessage
            );
        }

        public void Send(string message) {
            var body = Encoding.UTF8.GetBytes(message);

            var props = _channel.CreateBasicProperties();
            props.ContentType = _route.ContentType;

            _channel.BasicPublish(
                _route.Exchange,
                _route.RoutingKey,
                props,
                body
            );

            _logger.LogInformation(
                "[-] message published({route} as {type}): {message}",
                _route.Exchange,
                _route.ExchangeType,
                message
            );
        }

        public void Dispose() {
            _channel.Dispose();
        }
    }
}