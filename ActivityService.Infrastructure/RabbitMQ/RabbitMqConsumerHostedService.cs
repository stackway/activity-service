using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ActivityService.Infrastructure.RabbitMQ {
    public class RabbitMqConsumerHostedService<TM> : BackgroundService {
        private readonly IRabbitMqConsumer<TM> _consumer;
        private readonly ILogger _logger;

        public RabbitMqConsumerHostedService(
            IRabbitMqConsumer<TM> consumer,
            ILogger<RabbitMqConsumerHostedService<TM>> logger
        ) {
            _consumer = consumer;
            _logger = logger;

            _logger.LogInformation("hosted service is up");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken) {
            stoppingToken.ThrowIfCancellationRequested();

            return _consumer.SetupConsumerByStatus();
        }

        public override void Dispose() {
            _consumer.Dispose();

            base.Dispose();
        }
    }
}