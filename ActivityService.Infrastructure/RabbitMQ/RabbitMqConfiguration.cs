using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;

namespace ActivityService.Infrastructure.RabbitMQ {
    public static class RabbitMqConfiguration {
        public static IServiceCollection ConfigureRabbitMqConnection(
            this IServiceCollection services,
            IConfiguration configuration
        ) {
            var brokerConfig = BrokerConfig.From(configuration);

            services.AddSingleton<BrokerConfig>(brokerConfig);
            services.AddSingleton<IConnection>(
                new ConnectionFactory() {
                    HostName = brokerConfig.Host
                }.CreateConnection()
            );

            return services;
        }
    }
}