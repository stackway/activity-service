using Microsoft.Extensions.Configuration;

namespace ActivityService.Infrastructure.RabbitMQ {
    public class BrokerConfig {
        public readonly string Host;

        public BrokerConfig(string host) {
            Host = host;
        }

        public static BrokerConfig From(IConfiguration config) {
            return new BrokerConfig(config["MQ:BROKER_HOST"]);
        }
    }
}