using Microsoft.Extensions.DependencyInjection;

using Hangfire;
using Hangfire.MemoryStorage;

namespace ActivityService.Infrastructure.BackgroundProcess {
    public static class HangfireConfiguration {
        public static IServiceCollection ConfigureHangfire(
            this IServiceCollection services
        ) {
            services.AddHangfire(config => config
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseMemoryStorage()
            );

            services.AddHangfireServer();

            return services;
        }
    }
}