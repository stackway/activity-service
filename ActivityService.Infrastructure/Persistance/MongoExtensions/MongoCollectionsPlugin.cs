using System.Threading.Tasks;
using MongoDB.Driver;

namespace ActivityService.Infrastructure.Persistance.MongoExtensions {
    public static class MongoCollectionsPlugin {
        public static IFindFluent<T, T> FindAll<T>(
            this IMongoCollection<T> context
        ) {
            return context.Find(_ => true);
        }

        /// <summary>
        /// not the best solution for async queries,
        /// because async cursor is very breakable
        /// </summary>
        /// <param name="context">the mongo collection</param>
        /// <typeparam name="T">type of collection</typeparam>
        /// <returns>the async cursor for iterating over it's</returns>
        public static async Task<IAsyncCursor<T>> FindAllAsync<T>(
            this IMongoCollection<T> context
        ) {
            return await context.FindAsync(_ => true);
        }
    }
}