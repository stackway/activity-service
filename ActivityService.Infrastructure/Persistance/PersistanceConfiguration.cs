using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MongoDB.Driver;

namespace ActivityService.Infrastructure.Persistance {
    public static class PersistanceConfugration {
        public static IServiceCollection ConfigurePersistance(
            this IServiceCollection services,
            IConfiguration configuration
        ) {
            var mongoConfig = MongoDbConfig.From(configuration);

            services.AddSingleton<MongoDbConfig>(mongoConfig);
            services.AddScoped<MongoClient>(
                provider => new MongoClient(mongoConfig.ConnectionString)
            );
            services.AddScoped<IMongoDatabase>(
                provider => {
                    return provider
                        .GetService<MongoClient>()
                        .GetDatabase(mongoConfig.Database);
                }
            );

            return services;
        }

        public static IServiceCollection ConfigureDbContext<T>(
            this IServiceCollection services
        ) where T : class {
            return services.AddScoped<T>();
        }
    }
}