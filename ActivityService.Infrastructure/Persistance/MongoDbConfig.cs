using Microsoft.Extensions.Configuration;

namespace ActivityService.Infrastructure.Persistance {
    public class MongoDbConfig {
        public readonly string Database;
        public readonly string Host;
        public readonly int Port;
        public readonly string User;
        public readonly string Password;

        public MongoDbConfig(
            string database,
            string host,
            int port,
            string user,
            string password
        ) : this(database, host, port) {
            User = user;
            Password = password;
        }

        public MongoDbConfig(
            string database,
            string host,
            int port
        ) {
            Database = database;
            Host = host;
            Port = port;
        }

        public static MongoDbConfig From(IConfiguration config) {
            return new MongoDbConfig(
                config["MONGO:DB"],
                config["MONGO:HOST"],
                config.GetValue<int>("MONGO:PORT"),
                config["MONGO:USER"],
                config["MONGO:PASSWORD"]
            );
        }

        public string ConnectionString {
            get {
                var connection = $@"{Host}:{Port}";

                if (!string.IsNullOrEmpty(User) && !string.IsNullOrEmpty(Password))
                    connection = $@"{User}:{Password}@{connection}";

                return $@"mongodb://{connection}";
            }
        }
    }
}