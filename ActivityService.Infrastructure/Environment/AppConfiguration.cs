using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace ActivityService.Infrastructure.Environment {
    public static class AppConfiguration {
        public static IWebHostBuilder ApplyEnvironmentConfig(
            this IWebHostBuilder context
        ) {
            return context.ConfigureAppConfiguration((hostingContext, config) => {
                config
                    .AddEnvironmentVariables()
                    .AddEnvironmentVariables("PROVIDE_");
            });
        }
    }
}