using System;

namespace ActivityService.Application.Features.UserDeactivation {
    public interface ReadableDeactivationOptions {
        TimeSpan TimeToDeactivate { get; }
    }
}