using System;

using ActivityService.Domain.Entities;

namespace ActivityService.Application.Features.UserDeactivation {
    public class UserDeactivationHandler : IUserDeactivationHandler {
        private readonly ReadableDeactivationOptions _options;

        public UserDeactivationHandler(ReadableDeactivationOptions options) {
            _options = options;
        }

        public bool IsInactiveUser(UserActivity userActivity) {
            return IsInactiveUser(userActivity, _options.TimeToDeactivate);
        }

        public bool IsInactiveUser(UserActivity userActivity, TimeSpan timeToDeactivate) {
            if (DateTime.Now - userActivity.LastActivity > timeToDeactivate)
                return true;

            return false;
        }
    }
}