using System;

using Microsoft.Extensions.Configuration;

namespace ActivityService.Application.Features.UserDeactivation {
    public class DeactivationOptions : ReadableDeactivationOptions {
        public TimeSpan TimeToDeactivate { get; private set; }

        public DeactivationOptions(TimeSpan timeToDeactivate) {
            TimeToDeactivate = timeToDeactivate;
        }

        public static DeactivationOptions From(IConfiguration config) {
            return new DeactivationOptions(
                TimeSpan.ParseExact(
                    config["User:Deactivation"],
                    "c",
                    null
                )
            );
        }
    }
}