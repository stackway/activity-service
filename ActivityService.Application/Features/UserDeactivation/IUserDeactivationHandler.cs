using System;

using ActivityService.Domain.Entities;

namespace ActivityService.Application.Features.UserDeactivation {
    public interface IUserDeactivationHandler {
        bool IsInactiveUser(UserActivity userActivity);
        bool IsInactiveUser(
            UserActivity userActivity,
            TimeSpan timeToDeactivate
        );
    }
}