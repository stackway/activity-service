using System;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using ActivityService.Application.Models;
using ActivityService.Application.Repositories;
using Microsoft.Extensions.Logging;

namespace ActivityService.Application.Services {
    public interface IUserActivityService {
        void UpdateUserActivity(long id, string email, DateTime activityTime);
        Task UpdateUserActivityAsync(long id, string email, DateTime activityTime);

        Task UpdateTimeToUpdateResourcesAsync(long id, TimeSpan timeToUpdate);
        void UpdateTimeToUpdateResources(long id, TimeSpan timeToUpdate);
        Task UpdateTimeToUpdateSimilarUsersAsync(long id, TimeSpan timeToUpdate);
        void UpdateTimeToUpdateSimilarUsers(long id, TimeSpan timeToUpdate);

        /// <returns>`true` if update was successful</returns>
        bool ForceUpdateResources(long id, TimeSpan timeToUpdate);
    }

    public class UserActivityService : IUserActivityService {
        private readonly IUserActivityRepository _repository;
        private readonly ILogger _logger;

        public UserActivityService(
            IUserActivityRepository repository,
            ILogger<UserActivityService> logger
        ) {
            _repository = repository;
            _logger = logger;
        }

        public void UpdateUserActivity(
            long id,
            string email,
            DateTime activityTime
        ) {
            var foundActivities = _repository.Context.UsersActivities
                .Find(u => u.PersistanceId == id)
                .ToList();

            if (!foundActivities.Any()) {
                _repository.Add(
                    new UserActivityModel(id, email, activityTime)
                );

                return;
            }

            _repository.UpdateActivity(id, activityTime);
        }

        public async Task UpdateUserActivityAsync(
            long id,
            string email,
            DateTime activityTime
        ) {
            var foundActivities = await _repository.Context.UsersActivities
                .Find(u => u.PersistanceId == id)
                .ToListAsync();

            if (!foundActivities.Any()) {
                await _repository.AddAsync(
                    new UserActivityModel(id, email, activityTime)
                );

                return;
            }

            await _repository.UpdateActivityAsync(id, activityTime);
        }

        public async Task UpdateTimeToUpdateResourcesAsync(
            long id, TimeSpan timeToUpdate
        ) {
            await _repository.UpdateTimeToUpdateResourcesAsync(
                id,
                DateTime.Now + timeToUpdate
            );
        }

        public void UpdateTimeToUpdateResources(
            long id, TimeSpan timeToUpdate
        ) {
            _repository.UpdateTimeToUpdateResources(
                id,
                DateTime.Now + timeToUpdate
            );
        }

        public async Task UpdateTimeToUpdateSimilarUsersAsync(
            long id, TimeSpan timeToUpdate
        ) {
            await _repository.UpdateTimeToUpdateSimilarUsersAsync(
                id,
                DateTime.Now + timeToUpdate
            );
        }

        public void UpdateTimeToUpdateSimilarUsers(
            long id, TimeSpan timeToUpdate
        ) {
            _repository.UpdateTimeToUpdateSimilarUsers(
                id,
                DateTime.Now + timeToUpdate
            );
        }

        public bool ForceUpdateResources(long id, TimeSpan timeToUpdate) {
            if (!_repository.CanForceUpdateResources(id)) {
                _logger.LogWarning(
                    "can't force update resources for: {id}",
                    id
                );

                return false;
            }

            _repository.ForceUpdateResources(
                id,
                DateTime.Now + timeToUpdate
            );

            return true;
        }
    }
}