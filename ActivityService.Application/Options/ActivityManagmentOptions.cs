using System;

using Microsoft.Extensions.Configuration;

namespace ActivityService.Application.Options {
    public interface ActivityManagementOptions {
        TimeSpan ResourcesActivityTimeInterval { get; }
        TimeSpan SimilarUsersActivityTimeInterval { get; }
        TimeSpan ForceUpdateTimeInterval { get; }
    }

    public class BaseActivityManagementOptions : ActivityManagementOptions {
        public TimeSpan ResourcesActivityTimeInterval { get; private set; }
        public TimeSpan SimilarUsersActivityTimeInterval { get; private set; }
        public TimeSpan ForceUpdateTimeInterval { get; private set; }

        public BaseActivityManagementOptions(
            TimeSpan resourcesActivityTimeInterval,
            TimeSpan similarUsersActivityTimeInterval,
            TimeSpan forceUpdateTimeInterval
        ) {
            ResourcesActivityTimeInterval = resourcesActivityTimeInterval;
            SimilarUsersActivityTimeInterval = similarUsersActivityTimeInterval;
            ForceUpdateTimeInterval = forceUpdateTimeInterval;
        }

        public static ActivityManagementOptions From(IConfiguration config) {
            return new BaseActivityManagementOptions(
                TimeSpan.ParseExact(
                    config["Management:TimeInterval:UpdateResources"],
                    "c",
                    null
                ),
                TimeSpan.ParseExact(
                    config["Management:TimeInterval:UpdateSimilarUsers"],
                    "c",
                    null
                ),
                TimeSpan.ParseExact(
                    config["Management:TimeInterval:ForceUpdate"],
                    "c",
                    null
                )
            );
        }
    }
}