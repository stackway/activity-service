using Microsoft.Extensions.Configuration;

namespace ActivityService.Application.Options {
    public interface UsersOptions {
        int UsersToFind { get; }
    }

    public class BaseUsersOptions : UsersOptions {
        public int UsersToFind { get; private set; }

        public BaseUsersOptions(IConfiguration config) {
            UsersToFind = config.GetValue<int>("Service:UsersToFind");
        }
    }
}