using System;
using System.Linq;
using System.Threading.Tasks;

namespace ActivityService.Application.Handlers {
    public class ChainedHandler<T> : Handler<T> {
        private readonly Handler<T> _handler;
        private ChainedHandler<T> _pipeline;

        public ChainedHandler(
            Handler<T> handler,
            params Handler<T>[] handlers
        ) {
            _handler = handler;

            if (handlers.Length > 0) {
                Handler<T>[] restHandlers = new Handler<T>[] { };

                if (handlers.Length > 1) {
                    restHandlers = handlers.Skip(1).ToArray();
                }

                _pipeline = new ChainedHandler<T>(
                    handlers.First(),
                    restHandlers
                );
            }
        }

        /// <summary>
        ///     Add <paramref name="handler"/>  to the chain
        /// </summary>
        /// <param name="handler">
        ///     Will be the next handler in the chain
        /// </param>
        /// <returns>
        ///     The next chain <paramref name="handler"/>
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     Throw if <paramref name="handler"/> is null
        /// </exception>
        /// <exception cref="InvalidOperationException">
        ///     Throw if chain already has next handler
        /// </exception>
        public ChainedHandler<T> Add(Handler<T> handler) {
            if (handler == null)
                throw new ArgumentNullException();

            if (_pipeline != null)
                throw new InvalidOperationException();

            _pipeline = new ChainedHandler<T>(handler);

            return _pipeline;
        }

        public bool Handle(T resource) {
            var isSuccess = _handler.Handle(resource);

            if (_pipeline != null && isSuccess) {
                isSuccess = _pipeline.Handle(resource);
            }

            return isSuccess;
        }
    }
}