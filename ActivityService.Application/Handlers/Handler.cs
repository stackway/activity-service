using System.Threading.Tasks;

namespace ActivityService.Application.Handlers {
    public interface Handler<T> {
        bool Handle(T resource);
    }
}