using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using MongoDB.Driver;

using ActivityService.Application.Models;
using ActivityService.Infrastructure.Persistance.MongoExtensions;
using LanguageExt;

namespace ActivityService.Application.Repositories {
    public interface IUserActivityRepository {
        ActivityContext Context { get; }

        void Add(UserActivityModel activity);
        Task AddAsync(UserActivityModel activity);

        IEnumerable<UserActivityModel> FindAll();
        Task<IEnumerable<UserActivityModel>> FindAllAsync();

        Task<IEnumerable<UserActivityModel>> FindByRelevantAsync(
            DateTime relevantTime
        );

        IEnumerable<UserActivityModel> FindByRelevant(
            DateTime relevantTime
        );

        void UpdateActivity(long id, DateTime activityTime);
        Task UpdateActivityAsync(long id, DateTime activityTime);

        Task UpdateTimeToUpdateResourcesAsync(long id, DateTime timeToUpdate);
        void UpdateTimeToUpdateResources(long id, DateTime timeToUpdate);
        Task UpdateTimeToUpdateSimilarUsersAsync(long id, DateTime timeToUpdate);
        void UpdateTimeToUpdateSimilarUsers(long id, DateTime timeToUpdate);

        bool CanForceUpdateResources(long id);
        void ForceUpdateResources(long id, DateTime timeToUpdate);

        Option<UserActivityModel> FindById(long id);
        void Remove(long id);
    }

    public class UserActivityRepository : IUserActivityRepository {
        public ActivityContext Context { get; private set; }

        private ILogger _logger;

        public UserActivityRepository(
            ActivityContext context,
            ILogger<UserActivityRepository> logger
        ) {
            Context = context;
            _logger = logger;
        }

        public void Add(UserActivityModel activity) {
            Context
                .UsersActivities
                .InsertOne(activity);
        }

        public async Task AddAsync(UserActivityModel activity) {
            await Context
                .UsersActivities
                .InsertOneAsync(activity);
        }

        public IEnumerable<UserActivityModel> FindAll() {
            return Context
                .UsersActivities
                .FindAll()
                .ToEnumerable();
        }

        public async Task<IEnumerable<UserActivityModel>> FindAllAsync() {
            return await Context
                .UsersActivities
                .FindAll()
                .ToListAsync();
        }

        public async Task<IEnumerable<UserActivityModel>> FindByRelevantAsync(
            DateTime relevantTime
        ) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity =>
                    activity.DateToUpdateResources > relevantTime ||
                    activity.DateToUpdateSimilarUsers > relevantTime
                );

            return await Context
                .UsersActivities
                .Find(filter)
                .ToListAsync();
        }

        public IEnumerable<UserActivityModel> FindByRelevant(
            DateTime relevantTime
        ) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity =>
                    activity.DateToUpdateResources < relevantTime ||
                    activity.DateToUpdateSimilarUsers < relevantTime
                );

            return Context.UsersActivities
                .Find(filter)
                .ToList();
        }

        public void UpdateActivity(long id, DateTime activityTime) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var update = Builders<UserActivityModel>
                .Update
                .Set<DateTime>(activity => activity.LastActivity, activityTime);

            var options = new FindOneAndUpdateOptions<UserActivityModel>();

            Context
                .UsersActivities
                .FindOneAndUpdate(filter, update, options);
        }

        public async Task UpdateActivityAsync(long id, DateTime activityTime) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var update = Builders<UserActivityModel>
                .Update
                .Set<DateTime>(activity => activity.LastActivity, activityTime);

            var options = new FindOneAndUpdateOptions<UserActivityModel>();

            await Context
                .UsersActivities
                .FindOneAndUpdateAsync(filter, update, options);
        }

        public async Task UpdateTimeToUpdateResourcesAsync(long id, DateTime timeToUpdate) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var update = Builders<UserActivityModel>
                .Update
                .Set<DateTime>(
                    activity => activity.DateToUpdateResources, timeToUpdate
                );

            var options = new FindOneAndUpdateOptions<UserActivityModel>();

            await Context
                .UsersActivities
                .FindOneAndUpdateAsync(filter, update, options);
        }

        public void UpdateTimeToUpdateResources(long id, DateTime timeToUpdate) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var update = Builders<UserActivityModel>
                .Update
                .Set<DateTime>(
                    activity => activity.DateToUpdateResources, timeToUpdate
                );

            var options = new FindOneAndUpdateOptions<UserActivityModel>();

            Context
                .UsersActivities
                .FindOneAndUpdate(filter, update, options);
        }

        public async Task UpdateTimeToUpdateSimilarUsersAsync(long id, DateTime timeToUpdate) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var update = Builders<UserActivityModel>
                .Update
                .Set<DateTime>(
                    activity => activity.DateToUpdateSimilarUsers, timeToUpdate
                );

            var options = new FindOneAndUpdateOptions<UserActivityModel>();

            await Context
                .UsersActivities
                .FindOneAndUpdateAsync(filter, update, options);
        }

        public void UpdateTimeToUpdateSimilarUsers(long id, DateTime timeToUpdate) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var update = Builders<UserActivityModel>
                .Update
                .Set<DateTime>(
                    activity => activity.DateToUpdateSimilarUsers, timeToUpdate
                );

            var options = new FindOneAndUpdateOptions<UserActivityModel>();

            Context
                .UsersActivities
                .FindOneAndUpdate(filter, update, options);
        }

        public bool CanForceUpdateResources(long id) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var foundActivity = Context.UsersActivities
                .Find(filter)
                .ToList()
                .FirstOrDefault();

            if (foundActivity == null) {
                _logger.LogWarning(
                    "can't find user activity by persistance id: {id}",
                    id
                );

                return false;
            }

            return foundActivity.CanUseForceUpdate();
        }

        public void ForceUpdateResources(long id, DateTime timeToUpdate) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var update = Builders<UserActivityModel>
                .Update
                .Set<DateTime>(
                    activity => activity.LastActivity, timeToUpdate
                );

            var options = new FindOneAndUpdateOptions<UserActivityModel>();

            Context
                .UsersActivities
                .FindOneAndUpdate(filter, update, options);
        }

        public Option<UserActivityModel> FindById(long id) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var foundActivity = Context.UsersActivities
                .Find(filter)
                .ToList()
                .FirstOrDefault();

            if (foundActivity == null) {
                _logger.LogWarning(
                    "can't find user by persistence id: {id}",
                    id
                );

                return Option<UserActivityModel>.None;
            }

            return Option<UserActivityModel>.Some(foundActivity);
        }

        public void Remove(long id) {
            var filter = Builders<UserActivityModel>
                .Filter
                .Where(activity => activity.PersistanceId == id);

            var options = new FindOneAndDeleteOptions<UserActivityModel>();

            Context
                .UsersActivities
                .FindOneAndDelete(filter, options);
        }
    }
}