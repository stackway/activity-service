using System;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

using ActivityService.Domain.Entities;

namespace ActivityService.Application.Models {
    public class UserActivityModel : IReadableUserActivity, IChangeableUserActivity {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }

        [BsonElement("PersistanceId")]
        public long PersistanceId { get; set; }
        [BsonElement("Email")]
        public string Email { get; set; }

        [BsonElement("LastActivity")]
        public DateTime LastActivity { get; set; }

        [BsonElement("DateToUpdateSimilarUsers")]
        public DateTime DateToUpdateSimilarUsers { get; set; }
        [BsonElement("DateToUpdateResources")]
        public DateTime DateToUpdateResources { get; set; }
        [BsonElement("LastDateToForceUpdate")]
        public DateTime AviableDateToForceUpdate { get; set; }

        public UserActivityModel() { }
        public UserActivityModel(long id, string email, DateTime activityTime) {
            PersistanceId = id;
            Email = email;
            LastActivity = activityTime;
            DateToUpdateResources = activityTime;
            DateToUpdateSimilarUsers = activityTime;
            AviableDateToForceUpdate = activityTime;
        }

        public bool IsRelevant(DateTime relevantTime) {
            return DateToUpdateResources < relevantTime || DateToUpdateSimilarUsers < relevantTime;
        }

        public bool CanUseForceUpdate() {
            return AviableDateToForceUpdate < DateTime.Now;
        }
    }
}