using System;
using MongoDB.Driver;

namespace ActivityService.Application.Models {
    public interface DbContext { }

    public class ActivityContext : DbContext {
        public readonly IMongoDatabase Database;

        public IMongoCollection<UserActivityModel> UsersActivities =>
            Database.GetCollection<UserActivityModel>("UsersActivities");

        public ActivityContext(IMongoDatabase database) {
            Database = database;
        }

        public void SetupContext() {
            Database.CreateCollection("UsersActivities");
        }
    }
}