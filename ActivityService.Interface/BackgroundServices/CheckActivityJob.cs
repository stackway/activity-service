using System;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using ActivityService.Domain.Entities;
using ActivityService.Application.Models;
using ActivityService.Application.Repositories;
using ActivityService.Application.Features.UserDeactivation;
using ActivityService.Application.Handlers;
using ActivityService.Interface.MessageBus;
using Microsoft.Extensions.Logging;

namespace ActivityService.Interface.BackgroundServices {
    public class ActivityPipeline : ChainedHandler<UserActivity> {
        public ActivityPipeline(
            DeactivationMessageEmitter deactivationHandler,
            UpdateResourcesMessageEmitter updateResourcesHandler,
            UpdateSimilarUsersMessageEmitter updateSimilarUsersHandler
        ) : base(
            deactivationHandler,
            updateResourcesHandler,
            updateSimilarUsersHandler
        ) { }
    }

    public interface ICheckActivityJob {
        void ProcessUserActivity();
    }

    public class CheckActivityJobProfile : Profile {
        public CheckActivityJobProfile() {
            CreateMap<UserActivityModel, UserActivity>()
                .ConstructUsing(e => new UserActivity());

            CreateMap<UserActivity, UserActivityModel>()
                .ConstructUsing(e => new UserActivityModel());
        }
    }

    public class CheckActivityJob : ICheckActivityJob {
        private readonly IUserActivityRepository _repository;
        private readonly ActivityPipeline _pipeline;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public CheckActivityJob(
            IUserActivityRepository repository,
            ActivityPipeline pipeline,
            IMapper mapper,
            ILogger<CheckActivityJob> logger
        ) {
            _repository = repository;
            _pipeline = pipeline;
            _mapper = mapper;
            _logger = logger;
        }

        public void ProcessUserActivity() {
            _logger.LogInformation(
                "job for process users activity is active"
            );

            var activities = _repository.FindByRelevant(DateTime.Now);

            if (!activities.Any()) {
                _logger.LogInformation(
                "there are no active users"
            );
            }

            foreach (var activity in activities) {
                _pipeline.Handle(
                    _mapper.Map<UserActivityModel, UserActivity>(activity)
                );
            }
        }
    }
}