using System.Threading.Tasks;
using AutoMapper;
using ActivityService.Domain.Entities;
using ActivityService.Application.Handlers;
using ActivityService.Application.Features.UserDeactivation;
using ActivityService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.Logging;

namespace ActivityService.Interface.MessageBus {
    public interface DeactivationMessage {
        long UserId { get; }
        string UserEmail { get; }
    }

    public class BaseDeactivationMessage : DeactivationMessage {
        public long UserId { get; set; }
        public string UserEmail { get; set; }
    }

    public class DeactivationMessageProfile : Profile {
        public DeactivationMessageProfile() {
            CreateMap<UserActivity, BaseDeactivationMessage>()
                .ForMember(
                    d => d.UserId,
                    opt => opt.MapFrom(o => o.PersistanceId)
                )
                .ForMember(
                    d => d.UserEmail,
                    opt => opt.MapFrom(o => o.Email)
                );
        }
    }

    public class DeactivationMessageMqRoute : RabbitMqSenderRoute<DeactivationMessage> {
        public DeactivationMessageMqRoute() {
            Exchange = "deactivate-user";
        }
    }

    public interface DeactivationMessageEmitter : Handler<UserActivity> { }

    public class BaseDeactivationMessageEmitter : DeactivationMessageEmitter {
        private readonly IRabbitMqSender<DeactivationMessage> _sender;
        private readonly IUserDeactivationHandler _deactivation;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public BaseDeactivationMessageEmitter(
            IRabbitMqSender<DeactivationMessage> sender,
            IUserDeactivationHandler deactivation,
            IMapper mapper,
            ILogger<BaseDeactivationMessageEmitter> logger
        ) {
            _sender = sender;
            _deactivation = deactivation;
            _mapper = mapper;
            _logger = logger;
        }

        public bool Handle(UserActivity activity) {
            if (!_deactivation.IsInactiveUser(activity)) {
                return true;
            }

            _logger.LogInformation(
                "user is inactive: {email}",
                activity.Email
            );

            _sender.Send(
                _mapper.Map<UserActivity, BaseDeactivationMessage>(activity)
            );

            return false;
        }
    }
}