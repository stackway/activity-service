using System;
using System.Threading.Tasks;
using AutoMapper;
using ActivityService.Domain.Entities;
using ActivityService.Application.Handlers;
using ActivityService.Application.Options;
using ActivityService.Application.Services;
using ActivityService.Infrastructure.RabbitMQ;

namespace ActivityService.Interface.MessageBus {
    public interface UpdateSimilarUsersMessage {
        long Id { get; }
        string Email { get; }

        int CountToCollect { get; }
    }

    public class BaseUpdateSimilarUsersMessage : UpdateSimilarUsersMessage {
        public long Id { get; set; }
        public string Email { get; set; }

        public int CountToCollect { get; private set; }

        public BaseUpdateSimilarUsersMessage SetCountToCollect(
            int count
        ) {
            CountToCollect = count;

            return this;
        }

    }

    public class UpdateSimilarUsersMessageProfile : Profile {
        public UpdateSimilarUsersMessageProfile() {
            CreateMap<UserActivity, BaseUpdateSimilarUsersMessage>();
        }
    }

    public class UpdateSimilarUsersMqRoute : RabbitMqSenderRoute<UpdateSimilarUsersMessage> {
        public UpdateSimilarUsersMqRoute() {
            Exchange = "update-similar-users";
        }
    }

    public interface UpdateSimilarUsersMessageEmitter : Handler<UserActivity> { }

    public class BaseUpdateSimilarUsersMessageEmitter : UpdateSimilarUsersMessageEmitter {
        private readonly IRabbitMqSender<UpdateSimilarUsersMessage> _sender;
        private readonly IUserActivityService _service;
        private readonly ActivityManagementOptions _management;
        private readonly UsersOptions _options;
        private readonly IMapper _mapper;

        public BaseUpdateSimilarUsersMessageEmitter(
            IRabbitMqSender<UpdateSimilarUsersMessage> sender,
            IUserActivityService service,
            ActivityManagementOptions management,
            UsersOptions options,
            IMapper mapper
        ) {
            _sender = sender;
            _service = service;
            _management = management;
            _options = options;
            _mapper = mapper;
        }

        public bool Handle(UserActivity activity) {
            if (activity.DateToUpdateSimilarUsers > DateTime.Now)
                return true;

            _sender.Send(_mapper
                .Map<UserActivity, BaseUpdateSimilarUsersMessage>(activity)
                .SetCountToCollect(_options.UsersToFind)
            );

            _service.UpdateTimeToUpdateSimilarUsers(
                activity.PersistanceId,
                _management.SimilarUsersActivityTimeInterval
            );

            return true;
        }
    }
}