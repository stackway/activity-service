using Microsoft.Extensions.Logging;
using AutoMapper;
using ActivityService.Domain.Entities;
using ActivityService.Infrastructure.RabbitMQ;
using ActivityService.Application.Services;
using ActivityService.Application.Options;
using ActivityService.Application.Repositories;
using ActivityService.Application.Models;

namespace ActivityService.Interface.MessageBus {
    public class ForceUpdateResourcesMessage {
        public long Id { get; set; }
        public string Email { get; set; }
    }

    public class ForceUpdateResourcesMqRoute : RabbitMqConsumerRoute<ForceUpdateResourcesMessage> {
        public ForceUpdateResourcesMqRoute() {
            Queue = "activity-service.force-update-resources";
            Exchange = "force-update-resources";
        }
    }

    public class ForceUpdateResourcesConsumerService : IRabbitMqConsumerService<ForceUpdateResourcesMessage> {
        private readonly IUserActivityService _service;
        private readonly IUserActivityRepository _repository;
        private readonly UpdateResourcesMessageEmitter _emitter;
        private readonly ActivityManagementOptions _management;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public ForceUpdateResourcesConsumerService(
            IUserActivityService service,
            IUserActivityRepository repository,
            UpdateResourcesMessageEmitter emitter,
            ActivityManagementOptions management,
            IMapper mapper,
            ILogger<ForceUpdateResourcesConsumerService> logger
        ) {
            _service = service;
            _emitter = emitter;
            _repository = repository;
            _management = management;
            _mapper = mapper;
            _logger = logger;
        }

        public void ProcessMessage(ForceUpdateResourcesMessage message) {
            var isSuccessfulUpdate = _service.ForceUpdateResources(
                message.Id,
                _management.ForceUpdateTimeInterval
            );

            if (!isSuccessfulUpdate) {
                return;
            }

            _repository.FindById(message.Id)
                .Match(activity => {
                    _logger.LogInformation(
                        "proceed to force update resources for: {email}",
                        message.Email
                    );

                    _emitter.ForceHandle(
                        _mapper.Map<UserActivityModel, UserActivity>(activity)
                    );
                },
                    () => {
                        _logger.LogWarning(
                            "can't find user activity for: {email}",
                            message.Email
                        );
                    });
        }
    }
}