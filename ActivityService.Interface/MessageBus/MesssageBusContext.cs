using ActivityService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.DependencyInjection;

namespace ActivityService.Interface.MessageBus {
    public static class MessageBusContext {
        public static IServiceCollection ConfigureMqRoutes(this IServiceCollection services) {
            services
                .AddSingleton<IRabbitMqSenderRoute<UpdateSimilarUsersMessage>, UpdateSimilarUsersMqRoute>()
                .AddSingleton<IRabbitMqSenderRoute<UpdateResourcesMessage>, UpdateResourcesMqRoute>()
                .AddSingleton<IRabbitMqSenderRoute<DeactivationMessage>, DeactivationMessageMqRoute>();

            services
                .AddSingleton<IRabbitMqConsumerRoute<UserActivityMessage>, UserActivityMqRoute>()
                .AddSingleton<IRabbitMqConsumerRoute<ForceUpdateResourcesMessage>, ForceUpdateResourcesMqRoute>()
                .AddSingleton<IRabbitMqConsumerRoute<DeleteUserMessage>, DeleteUserMqRoute>();

            return services;
        }

        public static IServiceCollection ConfigureMqServices(this IServiceCollection services) {
            services
                .AddScoped<
                    IRabbitMqConsumerService<UserActivityMessage>,
                    UserActivityConsumerService
                >()
                .AddScoped<
                    IRabbitMqConsumerService<ForceUpdateResourcesMessage>,
                    ForceUpdateResourcesConsumerService
                >()
                .AddScoped<
                    IRabbitMqConsumerService<DeleteUserMessage>,
                    DeleteUserConsumerService
                >();

            return services;
        }

        public static IServiceCollection ConfigureMqSenders(this IServiceCollection services) {
            services
                .AddSingleton<IRabbitMqSender<UpdateSimilarUsersMessage>, RabbitMqSender<UpdateSimilarUsersMessage>>()
                .AddSingleton<IRabbitMqSender<UpdateResourcesMessage>, RabbitMqSender<UpdateResourcesMessage>>()
                .AddSingleton<IRabbitMqSender<DeactivationMessage>, RabbitMqSender<DeactivationMessage>>();

            return services;
        }

        public static IServiceCollection ConfigureMqConsumers(this IServiceCollection services) {
            services
                .AddScoped<
                    IRabbitMqConsumer<UserActivityMessage>,
                    RabbitMqConsumer<UserActivityMessage>
                >()
                .AddScoped<
                    IRabbitMqConsumer<ForceUpdateResourcesMessage>,
                    RabbitMqConsumer<ForceUpdateResourcesMessage>
                >()
                .AddScoped<
                    IRabbitMqConsumer<DeleteUserMessage>,
                    RabbitMqConsumer<DeleteUserMessage>
                >();

            return services;
        }

        public static IServiceCollection ConfigureMqHostedServices(this IServiceCollection services) {
            services
                .AddHostedService<RabbitMqConsumerHostedService<UserActivityMessage>>()
                .AddHostedService<RabbitMqConsumerHostedService<ForceUpdateResourcesMessage>>()
                .AddHostedService<RabbitMqConsumerHostedService<DeleteUserMessage>>();

            return services;
        }
    }
}