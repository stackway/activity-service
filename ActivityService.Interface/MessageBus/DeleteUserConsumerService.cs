using ActivityService.Application.Repositories;
using ActivityService.Infrastructure.RabbitMQ;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace ActivityService.Interface.MessageBus {
    public class DeleteUserMessage {
        public long Id { get; set; }
        public string Email { get; set; }
    }

    public class DeleteUserMqRoute : RabbitMqConsumerRoute<DeleteUserMessage> {
        public DeleteUserMqRoute() {
            Queue = "activity-service.delete-user";
            Exchange = "delete-user";
            ExchangeType = RabbitMqExchangeType.Fanout;
        }
    }

    public class DeleteUserConsumerService : IRabbitMqConsumerService<DeleteUserMessage> {
        private readonly IUserActivityRepository _repository;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public DeleteUserConsumerService(
            IUserActivityRepository repository,
            IMapper mapper,
            ILogger<DeleteUserConsumerService> logger
        ) {
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public void ProcessMessage(DeleteUserMessage message) {
            _repository.Remove(message.Id);

            _logger.LogInformation(
                "activity was removed for: {email}",
                message.Email
            );
        }
    }
}