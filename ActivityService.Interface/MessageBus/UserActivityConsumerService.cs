using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ActivityService.Application.Services;
using ActivityService.Infrastructure.RabbitMQ;

namespace ActivityService.Interface.MessageBus {
    public class UserActivityMessage {
        public long Id { get; set; }
        public string Email { get; set; }

        public DateTime ActivedAt { get; set; }
    }


    public class UserActivityMqRoute : RabbitMqConsumerRoute<UserActivityMessage> {
        public UserActivityMqRoute() {
            Queue = "activity-service.user-activity";
            Exchange = "user-activity";
        }
    }

    public class UserActivityConsumerService : IRabbitMqConsumerService<UserActivityMessage> {
        private readonly IUserActivityService _service;
        private readonly ILogger _logger;

        public UserActivityConsumerService(
            IUserActivityService service,
            ILogger<UserActivityConsumerService> logger
        ) {
            _service = service;
            _logger = logger;
        }

        public void ProcessMessage(UserActivityMessage message) {
            _service.UpdateUserActivity(
                message.Id,
                message.Email,
                message.ActivedAt
            );
        }
    }
}