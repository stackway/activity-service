using System;
using System.Threading.Tasks;
using AutoMapper;
using ActivityService.Domain.Entities;
using ActivityService.Application.Options;
using ActivityService.Application.Services;
using ActivityService.Application.Handlers;
using ActivityService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.Logging;

namespace ActivityService.Interface.MessageBus {
    public interface UpdateResourcesMessage {
        long UserId { get; }
        string UserEmail { get; }
    }

    public class BaseUpdateResourcesMessage : UpdateResourcesMessage {
        public long UserId { get; set; }
        public string UserEmail { get; set; }
    }

    public class UpdateResourceMessageProfile : Profile {
        public UpdateResourceMessageProfile() {
            CreateMap<UserActivity, BaseUpdateResourcesMessage>()
                .ForMember(
                    d => d.UserId,
                    opt => opt.MapFrom(o => o.PersistanceId)
                )
                .ForMember(
                    d => d.UserEmail,
                    opt => opt.MapFrom(o => o.Email)
                );
        }
    }

    public class UpdateResourcesMqRoute : RabbitMqSenderRoute<UpdateResourcesMessage> {
        public UpdateResourcesMqRoute() {
            Exchange = "update-resources";
        }
    }

    public interface UpdateResourcesMessageEmitter : Handler<UserActivity> {
        bool ForceHandle(UserActivity activity);
    }

    public class BaseUpdateResourcesMessageEmitter : UpdateResourcesMessageEmitter {
        private readonly IRabbitMqSender<UpdateResourcesMessage> _sender;
        private readonly IUserActivityService _service;
        private readonly ActivityManagementOptions _management;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public BaseUpdateResourcesMessageEmitter(
            IRabbitMqSender<UpdateResourcesMessage> sender,
            IUserActivityService service,
            ActivityManagementOptions management,
            IMapper mapper,
            ILogger<BaseUpdateResourcesMessageEmitter> logger
        ) {
            _sender = sender;
            _service = service;
            _management = management;
            _mapper = mapper;
            _logger = logger;
        }

        public bool Handle(UserActivity activity) {
            if (activity.DateToUpdateResources > DateTime.Now) {
                return true;
            }

            _sender.Send(_mapper.Map<UserActivity, BaseUpdateResourcesMessage>(
                activity
            ));

            _service.UpdateTimeToUpdateResources(
                activity.PersistanceId,
                _management.ResourcesActivityTimeInterval
            );

            return true;
        }

        public bool ForceHandle(UserActivity activity) {
            _sender.Send(_mapper.Map<UserActivity, BaseUpdateResourcesMessage>(
                activity
            ));

            _service.UpdateTimeToUpdateResources(
                activity.PersistanceId,
                _management.ResourcesActivityTimeInterval
            );

            return true;
        }
    }
}