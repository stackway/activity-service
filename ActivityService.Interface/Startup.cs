﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Hangfire;
using AutoMapper;
using ActivityService.Infrastructure.Persistance;
using ActivityService.Infrastructure.BackgroundProcess;
using ActivityService.Infrastructure.RabbitMQ;
using ActivityService.Application.Options;
using ActivityService.Application.Features.UserDeactivation;
using ActivityService.Application.Models;
using ActivityService.Application.Repositories;
using ActivityService.Application.Services;
using ActivityService.Interface.MessageBus;
using ActivityService.Interface.BackgroundServices;

namespace ActivityService.Interface {
    public class Startup {
        public Startup(IConfiguration configuration) { Configuration = configuration; }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services) {
            services.AddHealthChecks();

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services
                .ConfigurePersistance(Configuration)
                .ConfigureDbContext<ActivityContext>();

            services
                .AddScoped<IUserActivityRepository, UserActivityRepository>()
                .AddScoped<IUserActivityService, UserActivityService>();

            services.AddSingleton<IConfiguration>(Configuration);

            services.AddSingleton<ActivityManagementOptions>(
                _ => BaseActivityManagementOptions.From(Configuration)
            );

            services.AddSingleton<UsersOptions, BaseUsersOptions>();

            services.AddSingleton<ReadableDeactivationOptions>(
                _ => DeactivationOptions.From(Configuration)
            );

            services.AddScoped<IUserDeactivationHandler, UserDeactivationHandler>();

            services
                .AddScoped<DeactivationMessageEmitter, BaseDeactivationMessageEmitter>()
                .AddScoped<UpdateResourcesMessageEmitter, BaseUpdateResourcesMessageEmitter>()
                .AddScoped<UpdateSimilarUsersMessageEmitter, BaseUpdateSimilarUsersMessageEmitter>();

            services
                .AddScoped<ActivityPipeline>()
                .AddScoped<ICheckActivityJob, CheckActivityJob>();

            services.ConfigureRabbitMqConnection(Configuration);

            services
                .ConfigureMqRoutes()
                .ConfigureMqServices()
                .ConfigureMqSenders()
                .ConfigureMqConsumers()
                .ConfigureMqHostedServices();

            services.ConfigureHangfire();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            else app.UseHsts();

            app.UseHealthChecks(
                "/health",
                new HealthCheckOptions { Predicate = check => check.Tags.Contains("ready") }
            );

            app.UseHangfireServer();

            // better solution will be separate 
            // the job initialization to other abstraction 
            // and store cron in the config file (or env var) 
            // for now that cron mean every 10 minutes 
            RecurringJob.AddOrUpdate<ICheckActivityJob>(
                job => job.ProcessUserActivity(), "0 */1 * ? * *"
            );

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}