using System;

using ActivityService.Domain.Actions;

namespace ActivityService.Domain.Entities {
    public interface IReadableUserActivity {
        long PersistanceId { get; }
        string Email { get; }

        DateTime LastActivity { get; }

        DateTime DateToUpdateSimilarUsers { get; }
        DateTime DateToUpdateResources { get; }

        DateTime AviableDateToForceUpdate { get; }
    }

    public interface IChangeableUserActivity {
        long PersistanceId { get; set; }
        string Email { get; set; }

        DateTime LastActivity { get; set; }

        DateTime DateToUpdateSimilarUsers { get; set; }
        DateTime DateToUpdateResources { get; set; }

        DateTime AviableDateToForceUpdate { get; set; }
    }

    public class UserActivity : IReadableUserActivity, IChangeableUserActivity {
        public long PersistanceId { get; set; }
        public string Email { get; set; }

        public DateTime LastActivity { get; set; }

        public DateTime DateToUpdateSimilarUsers { get; set; }
        public DateTime DateToUpdateResources { get; set; }

        public DateTime AviableDateToForceUpdate { get; set; }

        public UserActivity() { }
        public UserActivity(long id, string email) {
            PersistanceId = id;
            Email = email;
            LastActivity = DateTime.Now;
            DateToUpdateSimilarUsers = DateTime.Now;
            DateToUpdateResources = DateTime.Now;
            AviableDateToForceUpdate = DateTime.Now;
        }

        public bool IsRelevant(DateTime relevantTime) {
            return DateToUpdateResources < relevantTime || DateToUpdateSimilarUsers < relevantTime;
        }

        public bool CanUseForceUpdate() {
            return AviableDateToForceUpdate < DateTime.Now;
        }

        public ActivityActions Actions() {
            return new ActivityActions(this);
        }
    }
}