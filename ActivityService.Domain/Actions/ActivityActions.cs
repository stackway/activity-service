using System;

using ActivityService.Domain.Entities;

namespace ActivityService.Domain.Actions {
    public class ActivityActions {
        private IChangeableUserActivity _activity;

        public ActivityActions(IChangeableUserActivity activity) {
            _activity = activity;
        }

        public ActivityActions UpdateLastActivityDate(DateTime activityDate) {
            _activity.LastActivity = activityDate;

            return this;
        }

        public IChangeableUserActivity Activity() {
            return _activity;
        }
    }
}