# Overview

## Resources

### DotNet

- <https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-timespan-format-strings>

### Cron

- <https://freeformatter.com/cron-expression-generator-quartz.html>

### Hangfire

- <https://stackoverflow.com/questions/41829993/hangfire-dependency-injection-with-net-core>
- <https://www.talkingdotnet.com/integrate-hangfire-with-asp-net-core-web-api/>
- <http://docs.hangfire.io/en/latest/>
- <https://indexoutofrange.com/Don't-do-it-now!-Part-6.-Hangfire-recurring-jobs/>
